from fastapi import FastAPI
from telethon.errors import PhoneNumberInvalidError, CodeInvalidError, FloodWaitError, PhoneCodeInvalidError, \
    PasswordHashInvalidError
from telethon.errors.rpcerrorlist import SessionPasswordNeededError, PhoneCodeExpiredError, AuthKeyUnregisteredError

from models import BaseResponse, UserUnregisteredError, FolderNotFoundError, ConcurrentProcessesOverSizeError, \
    BadRequestError
from models import Status


def flood_wait_exception_handler(request, exc):
    return BaseResponse.json_fail_response(Status.flood_wait, {"wait_seconds": exc.seconds})


def password_invalid_exception_handler(request, exc):
    return BaseResponse.json_fail_response(Status.invalid_code)


def telegram_client_unregistered_exception_handler(request, exc):
    return BaseResponse.json_fail_response(Status.user_unregistered_error)


def phone_code_expired_exception_handler(request, exc):
    return BaseResponse.json_fail_response(Status.code_expired)


def phone_code_invalid_exception_handler(request, exc):
    return BaseResponse.json_fail_response(Status.invalid_code)


def code_invalid_exception_handler(request, exc):
    return BaseResponse.json_fail_response(Status.invalid_code)


def session_password_needed_exception_handler(request, exc):
    return BaseResponse.json_fail_response(Status.two_fa_required)


def phone_number_invalid_exception_handler(request, exc):
    return BaseResponse.json_fail_response(Status.invalid_phone)


def telegram_user_folder_not_found_exception_handler(request, exc):
    return BaseResponse.json_fail_response(Status.folder_not_found)


def concurrent_processes_over_size_error_handler(request, exc):
    return BaseResponse.json_fail_response(Status.concurrency_over_size)


def bad_request_error_handler(request, exc: BadRequestError):
    return BaseResponse.json_fail_response(Status.bad_request, {'message': exc.message})


def register_exception_handlers(app: FastAPI):
    app.add_exception_handler(FloodWaitError, flood_wait_exception_handler)
    app.add_exception_handler(PasswordHashInvalidError, password_invalid_exception_handler)
    app.add_exception_handler(PhoneCodeExpiredError, phone_code_expired_exception_handler)
    app.add_exception_handler(PhoneCodeInvalidError, phone_code_invalid_exception_handler)
    app.add_exception_handler(CodeInvalidError, code_invalid_exception_handler)
    app.add_exception_handler(SessionPasswordNeededError, session_password_needed_exception_handler)
    app.add_exception_handler(PhoneNumberInvalidError, phone_number_invalid_exception_handler)
    app.add_exception_handler(AuthKeyUnregisteredError, telegram_client_unregistered_exception_handler)
    app.add_exception_handler(UserUnregisteredError, telegram_client_unregistered_exception_handler)
    app.add_exception_handler(FolderNotFoundError, telegram_user_folder_not_found_exception_handler)
    app.add_exception_handler(ConcurrentProcessesOverSizeError, concurrent_processes_over_size_error_handler)
    app.add_exception_handler(BadRequestError, bad_request_error_handler)
