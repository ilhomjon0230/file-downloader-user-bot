# File uploader telegram user bot

This project can help you upload file as document to telegram chat by user with api requests.

## Installation

1. **Clone the repository**:

   ```bash
   git clone https://gitlab.com/ilhomjon0230/file-downloader-user-bot.git
   cd file-downloader-user-bot
   ```

2. **Install Python 3.12**:

    - [Download Python 3.12](https://www.python.org/downloads/) if you haven't already.
    - You can use a package manager like Homebrew for macOS/Linux or Chocolatey for Windows.

3. **Install pipenv**:

   ```bash
   pip install pipenv
   ```

4. **Install project dependencies**:

   ```bash
   pipenv install -r requirements.txt
   ```

5. **Copy environment variables file**:

   ```bash
   cp .env.example .env
   ```

   Customize the `.env` file according to your environment configuration
6. **Create necessary tables and database**

   ```bash
   python3 dao.py
   ```
7. **Run project**

   ```bash
   uvicorn main:app --port=8080
   ```
   
8. **You can see swagger documentation by url** _http://localhost:8080/docs#/_

## Contributing

Contributions are welcome! If you find any issues or want to contribute to the project, feel free to open a pull request.

## License

This project isn't licensed
