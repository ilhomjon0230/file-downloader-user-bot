import asyncio

from fastapi import FastAPI, HTTPException, Depends
from fastapi.security import APIKeyHeader

from config import API_KEY, API_ID, API_HASH, DB_NAME
from exception_handler import register_exception_handlers
from models import *
from telegram_service import TelegramService
from dao import AsyncSQLiteDAO

app = FastAPI()
api_key_header = APIKeyHeader(name="X-API-Key")

db = AsyncSQLiteDAO(DB_NAME)
telegram_service = TelegramService(API_ID, API_HASH, db)
register_exception_handlers(app)


async def security_filter(api_key: str = Depends(api_key_header)):
    if api_key != API_KEY:
        raise HTTPException(status_code=403, detail=BaseResponse(success=False, status=Status.auth_error))
    return api_key


@app.post("/send_code/", response_model=BaseResponse)
async def send_code(phone_request: PhoneRequest, api_key: str = Depends(security_filter)):
    phone_number = phone_request.phone_number
    if not (await telegram_service.send_code_request(phone_number)):
        return BaseResponse.fail_response(Status.already_authorized)
    return BaseResponse.success_response()


@app.post("/sign_in/", response_model=BaseResponse)
async def sign_in(otp_request: OTPRequest, api_key: str = Depends(security_filter)):
    phone_number = otp_request.phone_number
    otp = otp_request.otp
    session_string = await telegram_service.sign_in_with_otp(phone_number, otp)
    return BaseResponse.success_response({"session": session_string})


@app.post("/sign_in_2fa/", response_model=BaseResponse)
async def sign_in_2fa(twofa_request: TwoFARequest, api_key: str = Depends(security_filter)):
    phone_number = twofa_request.phone_number
    password = twofa_request.password
    session_string = await telegram_service.sign_in_with_2fa(phone_number, password)
    return BaseResponse.success_response({"session": session_string})


@app.post("/send-message", response_model=BaseResponse)
async def send_message(send_file_request: SendMessageRequest, api_key: str = Depends(security_filter)):
    job_id = await telegram_service.create_job(send_file_request)
    return BaseResponse.success_response({"job_id": job_id})


@app.get("/progress", response_model=BaseResponse)
async def get_progress(job_id: int, api_key: str = Depends(security_filter)):
    status, progress, failed_reason = await telegram_service.get_job_progress(job_id)
    return BaseResponse.success_response({"status": status, "progress": progress, "failed_reason": failed_reason})

