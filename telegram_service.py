import os
import asyncio
from threading import Thread

import requests
from telethon import TelegramClient
from telethon.sessions import StringSession
from telethon.tl.types import Chat
from telethon.tl.types.auth import SentCode

from config import MAX_PROCESS_TIME
from dao import AsyncSQLiteDAO
from models import UserUnregisteredError, SendMessageRequest, ConcurrentProcessesOverSizeError, JobStatus, \
    ProcessNotFoundError, BadRequestError

job_processes = {}


def downloader(job_id, url: str, api_id, api_hash, send_file_request: SendMessageRequest, db: AsyncSQLiteDAO):
    job_processes[job_id] = {"status": JobStatus.downloading, "process": 0}

    async def update_job_status(status: JobStatus, failed_reason: str = None):
        await db.update_data('job', {'status': status.value, 'failed_reason': failed_reason}, {'id': job_id})

    asyncio.run(update_job_status(JobStatus.downloading))

    file_path = 'resources/' + url.split('/')[-1]
    try:
        r = requests.get(url, stream=True)
        chunk_size = 1024
        with open(file_path, 'wb') as f:
            total_length = int(r.headers.get('content-length'))
            for i, chunk in enumerate(r.iter_content(chunk_size=chunk_size)):
                if chunk:
                    f.write(chunk)
                    f.flush()
                progress = round((i + 1) * chunk_size / total_length * 100, 2)
                # print(f'\rDownloading {progress}%', end='')
                job_processes[job_id]["process"] = progress
        job_processes[job_id]["process"] = 100.00
    except Exception as e:
        asyncio.run(update_job_status(JobStatus.failed, str(e)))
        os.remove(file_path)
    # print()

    def callback(current, total):
        process = round(current / total * 100, 2)
        job_processes[job_id]["process"] = process
        # print(f"\rUploading {process}%", end='')

    async def upload_file():
        client: TelegramClient = TelegramClient(f"sessions/{send_file_request.phone_number}", api_id,
                                                api_hash)
        await client.connect()
        session = StringSession.save(client.session)
        client.disconnect()
        client: TelegramClient = TelegramClient(StringSession(session), api_id, api_hash)
        await client.connect()
        if not await client.is_user_authorized():
            client.disconnect()
            await update_job_status(JobStatus.failed)
            return
        target = send_file_request.chat_id or send_file_request.chat
        if not target:
            client.disconnect()
            await update_job_status(JobStatus.failed)
            return
        await client.send_file(entity=target, file=file_path, progress_callback=callback,
                               caption=send_file_request.caption,
                               force_document=send_file_request.force_document, reply_to=send_file_request.reply_to)
        client.disconnect()
        job_processes[job_id]["process"] = 100.00
        job_processes[job_id]["status"] = JobStatus.success
        await update_job_status(JobStatus.success)
        job_processes.pop(job_id)
        os.remove(file_path)

    job_processes[job_id]["status"] = JobStatus.uploading
    job_processes[job_id]["process"] = 0
    try:
        asyncio.run(update_job_status(JobStatus.uploading))
        asyncio.run(upload_file())
    except Exception as e:
        asyncio.run(update_job_status(JobStatus.failed, str(e)))
        os.remove(file_path)


class TelegramService:
    def __init__(self, api_id, api_hash, db: AsyncSQLiteDAO):
        self.api_id = api_id
        self.api_hash = api_hash
        self.db = db

    async def send_code_request(self, phone_number) -> bool:
        client: TelegramClient = TelegramClient(f"sessions/{phone_number}", self.api_id, self.api_hash)
        await self._connect_client(client)
        if not await client.is_user_authorized():
            sent_code: SentCode = await client.send_code_request(phone_number)
            with open(f"sessions/{phone_number}_hash.txt", "w") as hash_file:
                hash_file.write(sent_code.phone_code_hash)
            await self._disconnect_client(client)
            return True
        await self._disconnect_client(client)
        return False

    async def sign_in_with_otp(self, phone_number, otp) -> str:
        client: TelegramClient = TelegramClient(f"sessions/{phone_number}", self.api_id, self.api_hash)
        await self._connect_client(client)
        with open(f"sessions/{phone_number}_hash.txt", "r") as hash_file:
            phone_hash = hash_file.readline()
        await client.sign_in(phone_number, code=otp, phone_code_hash=phone_hash)
        await self._disconnect_client(client)
        return StringSession.save(client.session)

    async def sign_in_with_2fa(self, phone_number, password) -> str:
        client: TelegramClient = TelegramClient(f"sessions/{phone_number}", self.api_id, self.api_hash)
        await self._connect_client(client)
        await client.sign_in(phone_number, password=password)
        await self._disconnect_client(client)
        return StringSession.save(client.session)

    async def _connect_client(self, client: TelegramClient):
        if not client.is_connected():
            await client.connect()

    async def _disconnect_client(self, client: TelegramClient):
        if client.is_connected():
            await client.disconnect()

    async def create_job(self, send_message_request: SendMessageRequest):
        if not send_message_request.chat and not send_message_request.chat_id:
            raise BadRequestError("Chat or chat id is required")
        if not send_message_request.url and not send_message_request.caption:
            raise BadRequestError("The file url or caption is required")
        if len(job_processes) >= MAX_PROCESS_TIME:
            raise ConcurrentProcessesOverSizeError()
        client: TelegramClient = TelegramClient(f"sessions/{send_message_request.phone_number}", self.api_id,
                                                self.api_hash)
        await self._connect_client(client)
        if not await client.is_user_authorized():
            raise UserUnregisteredError()
        target = send_message_request.chat_id or send_message_request.chat
        try:
            target_entity = await client.get_entity(target)
        except Exception as e:
            await self._disconnect_client(client)
            raise BadRequestError(f"You cannot send the message to {target} this chat")
        if isinstance(target_entity, Chat) and not target_entity.admin_rights.post_messages:
            await self._disconnect_client(client)
            raise BadRequestError(f"You cannot send the message to {target} this chat")

        job_id = await self.db.insert_data('job', {
            'phone_number': send_message_request.phone_number,
            'url': send_message_request.url,
            'caption': send_message_request.caption,
            'chat': send_message_request.chat,
            'chat_id': send_message_request.chat_id,
            'reply_to': send_message_request.reply_to,
            'force_document': send_message_request.force_document,
            'status': JobStatus.starting.value
        })
        if send_message_request.url is None:
            try:
                await client.send_message(target_entity, send_message_request.caption,
                                          reply_to=send_message_request.reply_to)
                await self.db.update_data('job', {'status': JobStatus.success}, {'id': job_id})
            except Exception as e:
                await self._disconnect_client(client)
                await self.db.update_data('job', {'status': JobStatus.failed, 'failed_reason': str(e)},
                                          {'id': job_id})
            finally:
                return job_id
        await self._disconnect_client(client)
        thread = Thread(target=downloader,
                        args=(
                            job_id, send_message_request.url, self.api_id, self.api_hash, send_message_request,
                            self.db))
        thread.start()
        return job_id

    async def get_job_progress(self, job_id):
        result = await self.db.select_data_columns("job", ['id', 'status', 'failed_reason'], {"id": job_id})
        if len(result) <= 0:
            raise ProcessNotFoundError()
        _, status, failed_reason = result[0]
        if status == JobStatus.success.value or status == JobStatus.failed.value:
            return status, 100.00, failed_reason
        return job_processes[job_id]['status'], job_processes[job_id]['process'], failed_reason
