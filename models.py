from enum import Enum
from typing import Optional, Any

from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field


class Status(str, Enum):
    success = "success"
    user_unregistered_error = "user_unregistered_error"
    code_expired = "code_expired"
    already_authorized = "already_authorized"
    auth_error = "auth_error"
    invalid_phone = "invalid_phone"
    invalid_code = "invalid_code"
    flood_wait = "flood_wait"
    two_fa_required = "2fa_required"
    folder_not_found = "folder_not_found"
    concurrency_over_size = "concurrency_over_size"
    bad_request = "bad_request"


class JobStatus(str, Enum):
    success = "success"
    failed = "failed"
    downloading = "downloading"
    uploading = "uploading"
    starting = "starting"


class BaseResponse(BaseModel):
    success: bool
    status: Status
    data: Optional[Any] = None

    @staticmethod
    def success_response(data: Optional[Any] = None):
        return BaseResponse(success=True, status=Status.success, data=data)

    @staticmethod
    def fail_response(status: Status, data: Optional[Any] = None):
        return BaseResponse(success=False, status=status, data=data)

    @staticmethod
    def json_fail_response(status: Status, data: Optional[Any] = None):
        return JSONResponse(BaseResponse.fail_response(status, data).dict())


class PhoneRequest(BaseModel):
    phone_number: str


class OTPRequest(BaseModel):
    phone_number: str
    otp: str


class TwoFARequest(BaseModel):
    phone_number: str
    password: str


class SendMessageRequest(BaseModel):
    phone_number: str
    url: Optional[str] = None
    chat: Optional[str] = None
    chat_id: Optional[int] = None
    caption: Optional[str] = None
    reply_to: Optional[str] = None
    force_document: Optional[bool] = True


class EditMainFolderRequest(BaseModel):
    phone_number: str
    id: int


class EditAutoAnswerRequest(BaseModel):
    phone_number: str
    enabled: bool
    message: str


class EditFilterRequest(BaseModel):
    phone_number: str
    common_group_count: int = Field(gt=0)


class UserUnregisteredError(Exception):
    pass


class FolderNotFoundError(Exception):
    pass


class ConcurrentProcessesOverSizeError(Exception):
    pass


class ProcessNotFoundError(Exception):
    pass


class BadRequestError(Exception):
    def __init__(self, message: str, *args):
        self.message = message
        super().__init__(*args)
