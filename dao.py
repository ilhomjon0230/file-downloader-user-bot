import asyncio

import aiosqlite
from aiosqlite import Cursor

from config import DB_NAME


class AsyncSQLiteDAO:
    def __init__(self, db_name):
        self.db_name = db_name

    async def execute_query(self, query, params=None, fetchall=False):
        async with aiosqlite.connect(self.db_name) as db:
            cursor = await db.execute(query, params or ())
            await db.commit()
            if fetchall:
                return await cursor.fetchall()
            return await cursor.fetchone()

    async def create_table(self, table_name, columns):
        query = f"CREATE TABLE IF NOT EXISTS {table_name} ({', '.join(columns)})"
        await self.execute_query(query)

    async def insert_data(self, table_name, data: dict):
        columns = ', '.join(data.keys())
        values = ', '.join(['?' for _ in data.values()])
        query = f"INSERT INTO {table_name} ({columns}) VALUES ({values})"
        async with aiosqlite.connect(self.db_name) as db:
            cursor: Cursor = await db.execute(query, tuple(data.values()))
            await db.commit()
            return cursor.lastrowid

    async def update_data(self, table_name, update_data, conditions):
        set_clause = ', '.join([f"{key} = ?" for key in update_data.keys()])
        condition_clause = ' AND '.join([f"{key} = ?" for key in conditions.keys()])
        query = f"UPDATE {table_name} SET {set_clause} WHERE {condition_clause}"
        await self.execute_query(query, tuple(update_data.values()) + tuple(conditions.values()))

    async def delete_data(self, table_name, conditions):
        condition_clause = ' AND '.join([f"{key} = ?" for key in conditions.keys()])
        query = f"DELETE FROM {table_name} WHERE {condition_clause}"
        await self.execute_query(query, tuple(conditions.values()))

    async def select_data(self, table_name, conditions=None):
        condition_clause = ' AND '.join([f"{key} = ?" for key in conditions.keys()]) if conditions else ''
        query = f"SELECT * FROM {table_name} WHERE {condition_clause}" if condition_clause else f"SELECT * FROM {table_name}"
        return await self.execute_query(query, tuple(conditions.values()) if conditions else (), fetchall=True)

    async def select_data_columns(self, table_name, columns: list, conditions=None):
        condition_clause = ' AND '.join([f"{key} = ?" for key in conditions.keys()]) if conditions else ''
        query = f"SELECT {','.join(columns)} FROM {table_name} WHERE {condition_clause}" if condition_clause else f"SELECT * FROM {table_name}"
        return await self.execute_query(query, tuple(conditions.values()) if conditions else (), fetchall=True)


async def create_necessary_tables():
    db = AsyncSQLiteDAO(DB_NAME)
    await db.create_table('job', [
        'id INTEGER primary key',
        'phone_number TEXT not null',
        'url text',
        'chat text',
        'chat_id integer',
        'caption text',
        'reply_to text',
        'force_document boolean default true not null',
        'status text',
        'failed_reason text'
    ])


async def main():
    # Initialize the AsyncSQLiteDAO
    dao = AsyncSQLiteDAO('example.db')

    # Create table
    await dao.create_table('your_table_name',
                           ['id INTEGER PRIMARY KEY', 'column1 TEXT', 'column2 TEXT', 'column3 TEXT'])

    # Insert data
    data_to_insert = {'column1': 'value1', 'column2': 'value2', 'column3': 'value3'}
    await dao.insert_data('your_table_name', data_to_insert)

    # Update data
    update_data = {'column2': 'new_value2', 'column3': 'new_value3'}
    update_conditions = {'column1': 'value1'}
    await dao.update_data('your_table_name', update_data, update_conditions)

    # Delete data
    delete_conditions = {'column1': 'value1'}
    await dao.delete_data('your_table_name', delete_conditions)

    # Select data
    select_conditions = {'column2': 'new_value2'}
    result = await dao.select_data('your_table_name', select_conditions)
    print(result)


if __name__ == '__main__':
    asyncio.run(create_necessary_tables())
