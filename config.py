from dotenv import load_dotenv
import os

# Load environment variables from .env file
load_dotenv()

# Get API key from environment variables
API_KEY = os.getenv("API_KEY")
API_ID = os.getenv("API_ID")
API_HASH = os.getenv("API_HASH")
DB_NAME = os.getenv("DB_NAME")
MAX_PROCESS_TIME = int(os.getenv("MAX_PROCESS_TIME"))
